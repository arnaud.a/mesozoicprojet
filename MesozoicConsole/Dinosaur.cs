using System;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        protected string name;
        protected virtual string specie { get { return "Dinosaur"; } }
        protected int age;

        public Dinosaur(string name, int age)
        {
            this.name = name;
            this.age = age;
        }
        public virtual string sayHello()
        {
            if (this.age != 0)
            {
                string phrase = string.Format("Je suis {0} le {1}, j'ai {2}ans.", this.name, this.specie, this.age);
                return phrase;
            }
            else
            {
                string phrase = string.Format("Je suis {0} le {1}, je viens de naître", this.name, this.specie);
                return phrase;
            }
        }

        public string roar()
        {
            string grrr = string.Format("Grrr");
            return grrr;
        }

        public int getAge()
        {
            return this.age;
        }
        public void setAge(int newAge)
        {
            this.age = newAge;
        }
        public string getSpecie()
        {
            return this.specie;
        }
        /*/ public void setSpecie(string newSpecie)
         {
             this.specie=newSpecie;
         }*/
        public string getName()
        {
            return this.name;
        }

        public void setName(string newName)
        {
            this.name = newName;
        }

        public string hug(Dinosaur Dinosaur)
        {
            string hug = string.Format("Je suis {0} et je fais un calin à {1}.", this.getName(), Dinosaur.getName());
            return hug;
        }
        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.name.GetHashCode();
            hash ^= this.age.GetHashCode();
            hash ^= this.specie.GetHashCode();
            return hash;
        }


        public static bool operator ==(Dinosaur dino1, Dinosaur dino2)
        {
            return dino1.name == dino2.name && dino1.specie == dino2.specie && dino1.age == dino2.age;
        }

        public static bool operator !=(Dinosaur dino1, Dinosaur dino2)
        {
            return !(dino1 == dino2);
        }
        public override bool Equals(object obj)
        {
            return obj is Dinosaur && this == (Dinosaur)obj;
        }
    }
}
