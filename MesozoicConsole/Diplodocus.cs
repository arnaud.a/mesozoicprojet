using System;

namespace Mesozoic
{
    public class Diplodocus : Dinosaur {
        protected override string specie {get{return"Diplodocus";}}
        public Diplodocus(string name, int age):
        base(name, age)
        {
            this.name = name;
            this.age = age;
        }
        public override string sayHello(){
            if (this.age != 0)
            {
                string phrase = string.Format("Je suis {0} le {1}, j'ai {2}ans.",this.name,this.specie,this.age);
                return phrase;
            }
            else
            {
                string phrase = string.Format("Je suis {0} le {1}, je viens de naître",this.name,this.specie);
                return phrase;
            }
        }
        public override string ToString(){
            return string.Format("{0} = {{name : {1} age : {2}}}", base.ToString(),this.getName(),this.getAge());        
            }
    }
}