﻿using System;
using Mesozoic;
using System.Collections.Generic;

namespace MesozoicConsole
{
    class Program
    {

        static void Main(string[] args)
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Stegausaurus nessie = new Stegausaurus("Nessie", 11);
            TyrannosaurusRex samy  = new TyrannosaurusRex("Samy", 30);
            Triceratops arthur = new Triceratops("Arthur", 23);

            Diplodocus louis2 = new Diplodocus("Louis", 12);

            Console.WriteLine(louis == louis);
            louis2 = louis;
            Console.WriteLine(louis == louis2);
            Console.WriteLine(nessie);
            
            Console.WriteLine(louis.GetHashCode()); // 496495061
            Console.WriteLine(louis2.GetHashCode()); //496495061
            Console.WriteLine(nessie.GetHashCode()); //-420686026

            Horde LesZamis = new Horde();

            LesZamis.AddDino(louis); //Append dinosaur reference to end of list
            LesZamis.AddDino(nessie);
            LesZamis.AddDino(samy);
            LesZamis.AddDino(arthur);

            Console.WriteLine(LesZamis.getSize());
            //Iterate over our list
            LesZamis.hordePresentation();
            LesZamis.getList().RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(LesZamis.getSize());
            //Iterate over our list
            LesZamis.hordePresentation();

            LesZamis.DelDino(louis);

            Console.WriteLine(LesZamis.getSize());
            //Iterate over our list
            LesZamis.hordePresentation();

            Laboratory IngenL = new Laboratory();
            Dinosaur Jean = IngenL.createTrex("Jean");
            LesZamis.AddDino(Jean);
            LesZamis.hordePresentation();


        }
    }
}
