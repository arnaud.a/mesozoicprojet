using System;
using Mesozoic;

namespace MesozoicConsole
{
    public class Laboratory
    {
        public static TDinosaur CreateDinosaur<TDinosaur>(string name, int age=0) where TDinosaur: Dinosaur
        {
            return (TDinosaur)Activator.CreateInstance(typeof(TDinosaur), name, age);
        }
        public TyrannosaurusRex createTrex(string name)
        {
            TyrannosaurusRex newDino = new TyrannosaurusRex(name,0);
            return newDino;
        }
        public Triceratops createTriceratops(string name)
        {
            Triceratops newDino = new Triceratops(name,0);
            return newDino;
        }
        public Diplodocus createDiplodocus(string name, string specie)
        {
            Diplodocus newDino = new Diplodocus(name,0);
            return newDino;
        }
        public Stegausaurus createStegausaurus(string name, string specie)
        {
            Stegausaurus newDino = new Stegausaurus(name,0);
            return newDino;
        }
    }
}