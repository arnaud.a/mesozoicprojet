using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;
using MesozoicConsole;

namespace MesozoicTest
{
    public class UnitTestTREX
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            TyrannosaurusRex Tyrou = new TyrannosaurusRex("Tyrou", 12);

            Assert.Equal("Tyrou", Tyrou.getName());
            Assert.Equal("TyrannosaurusRex", Tyrou.getSpecie());
            Assert.Equal(12, Tyrou.getAge());
        }
        [Fact]
        public void TestDinosaurRoar()
        {
            TyrannosaurusRex Tyrou = new TyrannosaurusRex("Tyrou", 12);
            Assert.Equal("Grrr", Tyrou.roar());
        }
        [Fact]
        public void TestDinosaurSayHello()
        {
            TyrannosaurusRex Tyrou = new TyrannosaurusRex("Tyrou", 12);
            Assert.Equal("Je suis Tyrou le TyrannosaurusRex, j'ai 12ans.", Tyrou.sayHello());
        }
        [Fact]
        public void testSet()
        {
            TyrannosaurusRex Tyrou = new TyrannosaurusRex("Tyrou", 14);

            Tyrou.setName("Johnny");
            Tyrou.setAge(34);

            Assert.Equal("Johnny",Tyrou.getName());
            Assert.Equal(34,Tyrou.getAge());
        }
        [Fact]
        public void testHug()
        {
            TyrannosaurusRex Tyrou = new TyrannosaurusRex("Tyrou", 35);
            TyrannosaurusRex nessie = new TyrannosaurusRex("Nessie", 24);
            Assert.Equal("Je suis Tyrou et je fais un calin à Nessie.", Tyrou.hug(nessie));
        }

        [Fact]
        
        public void testToString()
        {
            TyrannosaurusRex Tyrou = new TyrannosaurusRex ("Tyrou", 35);
            Assert.Equal("Mesozoic.TyrannosaurusRex = {name : Tyrou age : 35}", Tyrou.ToString());
        }

        [Fact]
        public void testOperatorOverrideEQUALEQUAL()
        {
            TyrannosaurusRex Tyrou = new TyrannosaurusRex("Tyrou", 35);
            TyrannosaurusRex Tyrou2 = new TyrannosaurusRex("Tyrou", 35);
            Assert.True(Tyrou == Tyrou2);
        }

        [Fact]
        public void testOperatorOverrideDIFFERENTEQUAL()
        {
          TyrannosaurusRex Tyrou = new TyrannosaurusRex("Tyrou", 35);
            TyrannosaurusRex Tyrou2 = new TyrannosaurusRex("Tyrou", 31);
            Assert.True(Tyrou != Tyrou2);  
        }

        [Fact]
        public void testLaboratory()
        {
            TyrannosaurusRex Tyrou =Laboratory.CreateDinosaur<TyrannosaurusRex>("Tyrou");
            Assert.IsType<TyrannosaurusRex>(Tyrou);
            Assert.Equal("Tyrou", Tyrou.getName());
            Assert.Equal(0, Tyrou.getAge());
        }
    } 
}
