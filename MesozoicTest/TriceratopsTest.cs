using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;
using MesozoicConsole;

namespace MesozoicTest
{
    public class UnitTestTrice
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Triceratops Triso = new Triceratops("Triso", 12);

            Assert.Equal("Triso", Triso.getName());
            Assert.Equal("Triceratops", Triso.getSpecie());
            Assert.Equal(12, Triso.getAge());
        }
        [Fact]
        public void TestDinosaurRoar()
        {
            Triceratops Triso = new Triceratops("Triso", 12);
            Assert.Equal("Grrr", Triso.roar());
        }
        [Fact]
        public void TestDinosaurSayHello()
        {
            Triceratops Triso = new Triceratops("Triso", 12);
            Assert.Equal("Je suis Triso le Triceratops, j'ai 12ans.", Triso.sayHello());
        }
        [Fact]
        public void testSet()
        {
            Triceratops Triso = new Triceratops("Triso", 14);

            Triso.setName("Johnny");
            Triso.setAge(34);

            Assert.Equal("Johnny",Triso.getName());
            Assert.Equal(34,Triso.getAge());
        }
        [Fact]
        public void testHug()
        {
            Triceratops Triso = new Triceratops("Triso", 35);
            Triceratops nessie = new Triceratops("Nessie", 24);
            Assert.Equal("Je suis Triso et je fais un calin à Nessie.", Triso.hug(nessie));
        }

        [Fact]
        public void testToString()
        {
            Triceratops Triso = new Triceratops ("Triso", 35);
            Assert.Equal("Mesozoic.Triceratops = {name : Triso age : 35}", Triso.ToString());
        }
        [Fact]
        public void testOperatorOverrideEQUALEQUAL()
        {
            Triceratops Triso = new Triceratops("Triso", 35);
            Triceratops Triso2 = new Triceratops("Triso", 35);
            Assert.True(Triso == Triso2);
        }

        [Fact]
        public void testOperatorOverrideDIFFERENTEQUAL()
        {
          Triceratops Triso = new Triceratops("Triso", 35);
            Triceratops Triso2 = new Triceratops("Triso", 31);
            Assert.True(Triso != Triso2);  
        }

        [Fact]
        public void testLaboratory()
        {
            Triceratops Triso =Laboratory.CreateDinosaur<Triceratops>("Triso");
            Assert.IsType<Triceratops>(Triso);
            Assert.Equal("Triso", Triso.getName());
            Assert.Equal(0, Triso.getAge());
        }
    } 
}
