using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;
using MesozoicConsole;

namespace MesozoicTest
{
    public class UnitTestDiplo
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Diplodocus Diplo = new Diplodocus("Diplo", 12);

            Assert.Equal("Diplo", Diplo.getName());
            Assert.Equal("Diplodocus", Diplo.getSpecie());
            Assert.Equal(12, Diplo.getAge());
        }
        [Fact]
        public void TestDinosaurRoar()
        {
            Diplodocus Diplo = new Diplodocus("Diplo", 12);
            Assert.Equal("Grrr", Diplo.roar());
        }
        [Fact]
        public void TestDinosaurSayHello()
        {
            Diplodocus Diplo = new Diplodocus("Diplo", 12);
            Assert.Equal("Je suis Diplo le Diplodocus, j'ai 12ans.", Diplo.sayHello());
        }
        [Fact]
        public void testSet()
        {
            Diplodocus Diplo = new Diplodocus("Diplo", 14);

            Diplo.setName("Johnny");
            Diplo.setAge(34);

            Assert.Equal("Johnny",Diplo.getName());
            Assert.Equal(34,Diplo.getAge());
        }
        [Fact]
        public void testHug()
        {
            Diplodocus Diplo = new Diplodocus("Diplo", 35);
            Diplodocus nessie = new Diplodocus("Nessie", 24);
            Assert.Equal("Je suis Diplo et je fais un calin à Nessie.", Diplo.hug(nessie));
        }

        [Fact]
        public void testToString()
        {
            Diplodocus Diplo = new Diplodocus ("Diplo", 35);
            Assert.Equal("Mesozoic.Diplodocus = {name : Diplo age : 35}", Diplo.ToString());
        }

        [Fact]
        public void testOperatorOverrideEQUALEQUAL()
        {
            Diplodocus Diplo = new Diplodocus("Diplo", 35);
            Diplodocus Diplo2 = new Diplodocus("Diplo", 35);
            Assert.True(Diplo == Diplo2);
        }

        [Fact]
        public void testOperatorOverrideDIFFERENTEQUAL()
        {
          Diplodocus Diplo = new Diplodocus("Diplo", 35);
            Diplodocus Diplo2 = new Diplodocus("Diplo", 31);
            Assert.True(Diplo != Diplo2);  
        }

        [Fact]

        public void testOverrideEqual()
        {
            object obj = new Diplodocus("Diplo", 35);
            Diplodocus Diplo = new Diplodocus("Diplo", 35);
            Assert.IsType<Diplodocus>(obj);
            Assert.True(Diplo.Equals(obj));

        }

        [Fact]

        public void testLaboratory()
        {
            Diplodocus Diplo =Laboratory.CreateDinosaur<Diplodocus>("Diplo");
            Assert.IsType<Diplodocus>(Diplo);
            Assert.Equal("Diplo", Diplo.getName());
            Assert.Equal(0, Diplo.getAge());
        }
    } 
}
