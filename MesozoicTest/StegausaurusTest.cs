using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;
using MesozoicConsole;

namespace MesozoicTest
{
    public class UnitTestTStegausaurus
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Stegausaurus Stego = new Stegausaurus("Stego", 12);

            Assert.Equal("Stego", Stego.getName());
            Assert.Equal("Stegausaurus", Stego.getSpecie());
            Assert.Equal(12, Stego.getAge());
        }
        [Fact]
        public void TestDinosaurRoar()
        {
            Stegausaurus Stego = new Stegausaurus("Stego", 12);
            Assert.Equal("Grrr", Stego.roar());
        }
        [Fact]
        public void TestDinosaurSayHello()
        {
            Stegausaurus Stego = new Stegausaurus("Stego", 12);
            Assert.Equal("Je suis Stego le Stegausaurus, j'ai 12ans.", Stego.sayHello());
        }
        [Fact]
        public void testSet()
        {
            Stegausaurus Stego = new Stegausaurus("Stego", 14);

            Stego.setName("Johnny");
            Stego.setAge(34);

            Assert.Equal("Johnny",Stego.getName());
            Assert.Equal(34,Stego.getAge());
        }
        [Fact]
        public void testHug()
        {
            Stegausaurus Stego = new Stegausaurus("Stego", 35);
            Stegausaurus nessie = new Stegausaurus("Nessie", 24);
            Assert.Equal("Je suis Stego et je fais un calin à Nessie.", Stego.hug(nessie));
        }

        [Fact]
        public void testToString()
        {
            Stegausaurus Stegau = new Stegausaurus ("Stegau", 35);
            Assert.Equal("Mesozoic.Stegausaurus = {name : Stegau age : 35}", Stegau.ToString());
        }
        
        [Fact]
        public void testOperatorOverrideEQUALEQUAL()
        {
            Stegausaurus Stegau = new Stegausaurus("Stegau", 35);
            Stegausaurus Stegau2 = new Stegausaurus("Stegau", 35);
            Assert.True(Stegau == Stegau2);
        }

        [Fact]
        public void testOperatorOverrideDIFFERENTEQUAL()
        {
          Stegausaurus Stegau = new Stegausaurus("Stegau", 35);
            Stegausaurus Stegau2 = new Stegausaurus("Stegau", 31);
            Assert.True(Stegau != Stegau2);  
        }

        [Fact]
        public void testLaboratory()
        {
            Stegausaurus Stego =Laboratory.CreateDinosaur<Stegausaurus>("Stego");
            Assert.IsType<Stegausaurus>(Stego);
            Assert.Equal("Stego", Stego.getName());
            Assert.Equal(0, Stego.getAge());
        }
    } 
}
