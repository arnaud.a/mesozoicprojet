using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;
using MesozoicConsole;

namespace MesozoicTest
{
    public class UnitTest1
    {
       /*  [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.getName());
            Assert.Equal("Stegausaurus", louis.getSpecie());
            Assert.Equal(12, louis.getAge());
        }
        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }
        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12ans.", louis.sayHello());
        }
        [Fact]
        public void testSet()
        {
            Dinosaur louis = new Dinosaur("Louis", "Tricératops", 14);

            louis.setName("Johnny");
            louis.setSpecie("Pirate");
            louis.setAge(34);

            Assert.Equal("Johnny",louis.getName());
            Assert.Equal("Pirate", louis.getSpecie());
            Assert.Equal(34,louis.getAge());
        }
        [Fact]
        public void testHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Tricératops", 35);
            Dinosaur nessie = new Dinosaur("Nessie","Diplodocus", 24);
            Assert.Equal("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }

        [Fact]
        public void testHordeConstructor()
        {
            Horde AS = new Horde();
            Assert.IsType<List<Dinosaur>>(AS.getList());      
        }
        [Fact]
        public void testgetSize()
        {
            Horde AS = new Horde();
            Assert.Equal(0,AS.getSize());
        }

        [Fact]
        public void testAddDino()
        {
            Dinosaur Louis = new Dinosaur("Louis", "Stegausaurus", 8);
            Horde AS = new Horde();
            AS.AddDino(Louis);
            Assert.Equal("Louis",AS.getList()[0].getName());
        }
        [Fact]
        public void testGetDino()
        {
            Dinosaur Louis = new Dinosaur ("Louis","Stegausaurus", 8);
            Horde teamLouis = new Horde();
            teamLouis.AddDino(Louis);
            Assert.Equal("Louis", teamLouis.getDino(0).getName());
        }
     /*   [Fact]
        public void testHordePresentation()
        {
            Dinosaur Louis = new Dinosaur ("Louis","Stegausaurus", 8);
            Horde teamLouis = new Horde();
            string presentation;
            teamLouis.AddDino(Louis);
            presentation = teamLouis.hordePresentation();
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 8ans.", teamLouis.hordePresentation());
        }  

        [Fact]
        public void LaboratoryTest()
        {
            Laboratory Ingen = new Laboratory();
            Assert.IsType<Laboratory>(Ingen);

        }
        [Fact]
        public void LaboratoryCreateDinoTest()
        {
            Laboratory Ingen = new Laboratory();
            Dinosaur Jean = Ingen.createDinosaur("Jean", "Arthusaurus");
            Assert.IsType<Dinosaur>(Jean);
            Assert.Equal("Jean", Jean.getName());
        }
        */
    } 
}
